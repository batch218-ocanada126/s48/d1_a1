/*
	GitBash:
	npm init -y
	npm install express
	npm i mongoose
	npm install cors
*/

// dependencies
const express = require("express");
const mongoose = require("mongoose");

// to create a express server/application
const app = express();

// Middlewares - allows to bridge our backend application(server) to our front end
// to allow cross origin resource sharing
app.use(cors());
// to read json objects
app.use(express.json());
// to read forms
app.use(express.urlencoded({extended:true}));

const port = 4000

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.1ugdi3t.mongodb.net/courseBooking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Prompts a message once connected
mongoose.connection.once('open', ()=> console.log('Now connected to Ocanada-Mongo DB Atlas. '));

// Prompts a meesage once connected to port 4000
app.listen(process.env.PORT || 4000, () => {console.log(`API is now online on port ${process.env.PORT || 4000}`)});
// 3000, 4000, 5000, 8000 - Port numbers for web applications


