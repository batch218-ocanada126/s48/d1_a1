

const mongoose = require("mongoose");



const user = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "FirstName is required"]
	},
	lastName: {
		type: String,
		required: [true, "LastName is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		defualt: true
	},
	mobileNo: {
		type: Number,
		required: [true, "MobileNumber is required"]
	},
	enrollments : [
			{
				courseId: {
					type: String,
					required: [true, "UserId is required"]
				},
				enrolledOn: {
					type: Date,
					defualt: new Date()
				},
				status: {
					type: String,
					defualt: "Enrolled"
				}
			}


	]


})