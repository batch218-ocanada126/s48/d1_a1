let http = require("http");   

http.createServer(function (request, response){

//1
	if(request.url == "/" && request.method == "GET"){


		response.writeHead(200, {"Content-Type" : "text/plain"});

		
		response.end("Welcome to booking system");
	}
//2
	if(request.url == "/profile" && request.method == "GET"){


		response.writeHead(200, {"Content-Type" : "text/plain"});

		
		response.end("Welcome to your profile");
	}

//3	
	if(request.url == "/courses" && request.method == "GET"){


		response.writeHead(200, {"Content-Type" : "text/plain"});

		// Ends the response process
		response.end("Here's our courses available");
	}

//4
	if(request.url == "/addCourse" && request.method == "POST"){


		response.writeHead(200, {"Content-Type" : "text/plain"});

		// Ends the response process
		response.end("Add course to our resources");
	}

//5
	if(request.url == "/updateCourse" && request.method == "PUT"){


		response.writeHead(200, {"Content-Type" : "text/plain"});

		// Ends the response process
		response.end("Update a course to our resources");
	}

//6
	if(request.url == "/archiveCourse" && request.method == "DELETE"){


		response.writeHead(200, {"Content-Type" : "text/plain"});

		// Ends the response process
		response.end("Archive courses to our resources");
	}



}).listen(4000); // port# 3000,4000,5000,8000 - port numbers that could be use in creating web application

console.log(`Server is running at localhost: 4000`);



// 1. Create a server response - "Welcome to booking system" to a GET method request accessing localhost:4000
// Screenshot your postman as proof it is working and the method request is right

// 2. Create a server response - "Welcome to your profile" to a GET method request accessing localhost:4000/profile
// Screenshot your postman as proof it is working and the method request is right

// 3. Create a server response - "Here's our courses available" to a GET method request accessing localhost:4000/courses
// Screenshot your postman as proof it is working and the method request is right

// 4. Create a server response - "Add course to our resources" to a POST method request accessing localhost:4000/addCourse
// Screenshot your postman as proof it is working and the method request is right

// 5. Create a server response - "Update a course to a database" to a PUT method request accessing localhost:4000/updateCourse
// Screenshot your postman as proof it is working and the method request is right

// 6. Create a server response - "Archive courses to our resources" to a DELETE method request accessing localhost:4000/archiveCourse
// Screenshot your postman as proof it is working and the method request is right
